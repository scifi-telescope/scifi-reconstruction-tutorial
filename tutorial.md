# SciFi Reconstruction tutorial
## Setup environment
0. Make sure you have the `scifi-rec` repository on your system and that you can compile correctly.
1. Clone this repository.
2. Update the `setup.sh` file with the correct position of `scifi-rec` on your system.
3. Run `source setup.sh`.

## Coordinates system
Two different coordinates systems are defined a global one (x, y, z, t) and a local one (u, v, w, s) on each sensor.

The global coordinates system is describes as follows:
* z is the direction of the beam
* y is towards the top
* x forms a right-handed coordinates system with y and z (so looking downstream, it points left)
* t is the time

The local coodinates system is defined as follows:
* u is the "x" and goes along the channels
* v is the "y" and goes along the fibre
* w is perpendicular to u and v, forming a right-handed system (less important)
* s is the local time, usually t +/- an offset (unless the sensor is moving really fast, but this option is not implemented yet)

Furthermore, 3 angles are defined in the local system:
* alpha is a rotation around u
* beta is a rotation around v
* gamma is a rotation around w

Given the three angles, the rotations are applied in the gamma->beta->alpha order.

**SHOW PICTURE**

The units are not specified anywhere (except for angles, which are radians). It is just required to be consistent and always use the same (for example mm and ns) in sensors definition, geometry, etc.

## Reconstruction
The reconstruction is articulated in several steps with the idea that the first ones (calibration and conversion) are run less often than e.g. alignment or tracking, especially while we are developing this software.

Currently the following features are more or less ready:
* Calibration
* Conversion to ROOT files
* Coarse alignment, using correlation (in time and space), to be verified
* Clusterizers
* Fine alignment, using tracks, to be verified
* Tracking with straight tracks, to be verified

To be done:
* Correction of hardware time delays (per channel)
* Per-channel time calibration using tracks
* An output file useful for further analysis
* Precise channel to position conversion

Just to be clear, reconstruction != analysis: here we aim at creating some intermediate processed file, containing hits, clusters, tracks that then will be used for further analysis. Some plots are still produced, but they are ugly and mostly to check that the reconstruction is working fine.

I think splitting the work in this way makes it easier to understand and control the different steps. If the reconstruction is done well, it makes the analysis itself much simpler and faster.

The reconstruction is currently structured in this way:
* Conversion of the raw data files while applying the time calibration
    * Input: the raw data file + calibration
    * Output: the converted data files + a histograms file to check the conversion
* Alignment coarse in space (x and y only), using clusters correlations between various planes. This is probably useless here, since the initial geometry is already quite precise
    * Input: data file
    * Ouput: updated geometry file + histograms
* Alignment coarse in time, using clusters correlations between various planes. Probably not needed.
    * Input: data file + geometry file from previous step
    * Ouput: updated geometry file + histograms
* Fine alignment using tracks (6 DOF). This also calculates the beam parameters. Iterative process, broken at the moment (at least if we use cosmcis data)
    * Input: data file + geometry file from previous step
    * Ouput: updated geometry file + histograms
* Reconstruction. Tracks are finally calculated and everything is written in a dedicated file (format to be decided, right now is not ideal):
    * Input: data file + aligned geometry
    * Ouput: data + histograms

### Configuration files
The different reconstruction tools expect 3 configuration files to be present. TOML format is used.
* analysis: it contains one or more entries for each tool (calibrate, convert, align...), determining how it will run. We will see details later

**open and comment analysis file**

* device: it contains two parts: first the sensors definition, describing the sensors used in the telescope (n. of channels, pitch, etc) and second the list of the sensors used, along with their name

**open and comment device file**
* geometry: given a device file, it describes the position and rotation, along with beam parameters as slope, divergence, energy and mass.

**open and comment geometry files**

* the `id` is determined by the device file, **starting at 0**
* the `offset` is the displacement wrt the origin in the global coordinates system (usually the first plane) in x, y, z and t
* `unit_u` and `unit_v` represents the orientation of u and v in the global space.

### Conversion
Raw data files are converted using `sfr-convert`.

**open conf file and comment**

The command to run is `sfr-convert -u all raw_data/run_000025 converted_data/run_000025 --verbose`.

**comment log from the tool**
**open output files and comment**

### Alignment
Let's skip this for now

### Reconstruction
Tracks reconstruction is performed using `sfr-convert`.

**open conf file and comment**

The command to run is `sfr-reconstruct -u 3d converted_data/run_000025.root reconstructed_data/run_000025 --verbose`.

**comment log from the tool**
**open output files and comment**

## The python module
Writing all these commands is quite tedious and error-prone. For this reason, a python module is available to call these tools from python.

The example file runs the full reconstruction for a given run.

```
./reco.py 25
```