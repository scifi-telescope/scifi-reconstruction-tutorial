#!/usr/bin/env python3

import scifi_rec as sfr
import argparse
import os.path as op
from os import makedirs
import pandas as pd

def main():
    parser = argparse.ArgumentParser('Run reconstruction for a single run')
    parser.add_argument('run', type=int, help='Run number to be processed')
    parser.add_argument('--verbose', '-v', action='store_true')
    args = parser.parse_args()

    run_reconstruction(args.run, verbose=args.verbose)


def run_reconstruction(run, raw_data_path='raw_data/run_{:06d}', converted_data_path='converted_data', calibration_path='calibration_files', geometry_path='geometry_files', output_path='reconstructed_data', **kwargs):
    """ Runs the reconstruction.

    The reconstruction is structured in the following way:
    1. calibration files are generated using sfr-calibrate and reading raw data directly (it's not necessary to convert them to root files beforehand. They are relatively small, so are quicly read by the raw txt parser and this is run one anyway)
    2. raw data files are converted to ROOT files and the calibration is applied during conversion
    3. Coarse alignemnt. A coarse alignment time based on correlations is run.
    4. Fine alignment. alignment based on local chi^2 is run and the final geometry file is generated
    5. Reconstruction. Hits are filtered here, then clusters and tracks are calculated

    """
    # try:
    #     makedirs(op.join(calibration_path, 'run_{:06d}'.format(run)))
    # except OSError:
    #     pass

    
    device = 'device.toml'
    geo = 'geometry.toml'
    # geo = op.join(geometry_path, 'run_{:06d}-geo.toml'.format(run))
    # geo = op.join('geometry.toml'.format(run))

    sfr.convert(
        raw_data_path.format(run),
        op.join(converted_data_path, 'run_{:06d}'.format(run)),
        subsection='all',
        device=device,
        geometry=geo,
        **kwargs
        )
    # sfr.align(
    #     op.join(converted_data_path, 'run_{:06d}.root'.format(run)),
    #     op.join(geometry_path, 'run_{:06d}_x'.format(run)),
    #     subsection='x',
    #     # num_events=100,
    #     device=device,
    #     **kwargs
    # )
    # sfr.align(
    #     op.join(converted_data_path, 'run_{:06d}.root'.format(run)),
    #     op.join(geometry_path, 'run_{:06d}_y'.format(run)),
    #     subsection='y',
    #     # num_events=100,
    #     device=device,
    #     **kwargs
    # )
    # sfr.align(
    #     op.join(converted_data_path, 'run_{:06d}.root'.format(run)),
    #     op.join(geometry_path, 'run_{:06d}_time'.format(run)),
    #     subsection='time',
    #     # num_events=100,
    #     device=device,
    #     **kwargs
    # )
    # sfr.align(
    #     op.join(converted_data_path, 'run_{:06d}.root'.format(run)),
    #     op.join(geometry_path, 'run_{:06d}'.format(run)),
    #     subsection='fine',
    #     # num_events=1000,
    #     device=device,
    #     #geometry=op.join(geometry_path, 'run_{:06d}-geo.toml'.format(run)),
    #     geometry=geo,
    #     **kwargs
    # )
    # sfr.align(
    #     op.join(converted_data_path, 'run_{:04d}.root'.format(run)),
    #     op.join(geometry_path, 'run_{:04d}_uvs'.format(run)),
    #     subsection='fine_uvs',
    #     num_events=1000,
    #     device=device,
    #     geometry=op.join(geometry_path, 'run_{:04d}_time-geo.toml'.format(run)),
    #     **kwargs
    # )
    # sfr.reconstruct(
    #     op.join(converted_data_path, 'run_{:04d}.root'.format(run)),
    #     op.join(output_path, 'run_{:04d}'.format(run)),
    #     subsection='showers',
    #     skip_events=1000,
    #     num_events=1000000000,
    #     device=device,
    #     geometry=geo,
    #     **kwargs
    # )
    sfr.reconstruct(
        op.join(converted_data_path, 'run_{:06d}.root'.format(run)),
        op.join(output_path, 'run_{:06d}'.format(run)),
        subsection='3d',
        skip_events=0,
        num_events=1000000000,
        device=device,
        geometry=geo,
        **kwargs
    )
    # sfr.reconstruct(
    #     op.join(converted_data_path, 'run_{:04d}.root'.format(run)),
    #     op.join(output_path, 'run_{:04d}_time'.format(run)),
    #     subsection='3d_time',
    #     skip_events=1000,
    #     num_events=1000000000,
    #     device=device,
    #     geometry=op.join(geometry_path, 'run_1011-geo.toml'),
    #     #geometry=op.join(geometry_path, 'run_{:04d}-geo.toml'.format(run)),
    #     **kwargs
    # )


if __name__ == '__main__':
    main()
